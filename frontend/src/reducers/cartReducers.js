import {CART_ADD_ITEM,
    CART_REMOVE_ITEM,
    CART_SAVE_SHIPPING_ADDRESS,

    CART_SAVE_PAYMENT_METHOD,

    CART_CLEAR_ITEMS,

    CART_NOVAPOSHTA_AREAS_REQUEST,
    CART_NOVAPOSHTA_AREAS_SUCCESS,
    CART_NOVAPOSHTA_AREAS_FAIL,

    CART_NOVAPOSHTA_CITIES_REQUEST,
    CART_NOVAPOSHTA_CITIES_SUCCESS,
    CART_NOVAPOSHTA_CITIES_FAIL,

    CART_NOVAPOSHTA_WAREHOUSES_SUCCESS,
    CART_NOVAPOSHTA_WAREHOUSES_REQUEST,
    CART_NOVAPOSHTA_WAREHOUSES_FAIL,
} from '../constants/cartConstants';



export const cartReducer = (state={
                                    cartItems: [],
                                    shippingAddress: {},
                                    paymentMethod: null,
                                   },
                            action) => {
    switch(action.type){
        case CART_ADD_ITEM:
            const item = action.payload
            const existItem = state.cartItems.find(x => x.product === item.product)

            if (existItem){
                return {
                    ...state,
                    cartItems: state.cartItems.map(x =>
                        x.product === existItem.product ? item: x
                    )
                }
            }else{
                return {
                        ...state,
                        cartItems: [...state.cartItems, item],
                    }
                }
        case CART_REMOVE_ITEM:
            return{
                ...state,
                cartItems: state.cartItems.filter( x => x.product !== action.payload)
            }

        case CART_SAVE_SHIPPING_ADDRESS:
            return {
                ...state,
                shippingAddress: action.payload,
            }

        case CART_SAVE_PAYMENT_METHOD:
            return {
                ...state,
                paymentMethod: action.payload,
            }

        case CART_CLEAR_ITEMS:
            return {
                ...state,
                cartItems: [],
            }

        case CART_NOVAPOSHTA_AREAS_REQUEST:
            return {
                ...state,
                novaposhtaAreas: {
                    loading: true,
                    error: false,
                    areasList: [],
                }
            }
        case CART_NOVAPOSHTA_AREAS_SUCCESS:
            return {
                ...state,
                novaposhtaAreas: {
                    loading: false,
                    error: false,
                    areasList: action.payload
                }

            }
        case CART_NOVAPOSHTA_AREAS_FAIL:
            return {
                ...state,
                novaposhtaAreas: {
                    loading: false,
                    error: action.payload,
                    areasList: []
                }
            }

        case CART_NOVAPOSHTA_CITIES_REQUEST:
            return {
                loading: true,
                ...state,
                novaposhtaCities: []
            }
        case CART_NOVAPOSHTA_CITIES_SUCCESS:
            return {
                loading: false,
                ...state,
                novaposhtaCities: action.payload,
            }
        case CART_NOVAPOSHTA_CITIES_FAIL:
            return {
                loading: false,
                ...state,
            }

        default:
            return state;
    }
}

export const novaPoshtaReducer = (state={
                                    areas: {},
                                    cities: {},
                                    warehouses: {}
                                    }, action) => {
    switch(action.type){
        case CART_NOVAPOSHTA_AREAS_REQUEST:
            return {
                ...state,
                areas: {
                    loading: true,
                    error: false,
                    areasList: [],
                }
            }
        case CART_NOVAPOSHTA_AREAS_SUCCESS:
            return {
                ...state,
                areas: {
                    loading: false,
                    error: false,
                    areasList: action.payload
                }

            }
        case CART_NOVAPOSHTA_AREAS_FAIL:
            return {
                ...state,
                areas: {
                    loading: false,
                    error: action.payload,
                    areasList: []
                }
            }

        case CART_NOVAPOSHTA_CITIES_REQUEST:
            return {
                ...state,
                cities: {
                    loading: true,
                    citiesList: [],
                    error: false,
                }

            }
        case CART_NOVAPOSHTA_CITIES_SUCCESS:
            return {
                ...state,
                cities: {
                    loading: false,
                    citiesList: action.payload,
                    error: false
                }

            }
        case CART_NOVAPOSHTA_CITIES_FAIL:
            return {
                ...state,
                cities: {
                    loading: false,
                    error: action.payload,
                    citiesList: []
                }
            }

        case CART_NOVAPOSHTA_WAREHOUSES_REQUEST:
            return {
                ...state,
                warehouses: {
                    loading: true,
                    warehousesList: [],
                    error: false,
                }

            }
        case CART_NOVAPOSHTA_WAREHOUSES_SUCCESS:
            return {
                ...state,
                warehouses: {
                    loading: false,
                    warehousesList: action.payload,
                    error: false
                }

            }
        case CART_NOVAPOSHTA_WAREHOUSES_FAIL:
            return {
                ...state,
                warehouses: {
                    loading: false,
                    error: action.payload,
                    warehousesList: []
                }
            }
        default:
            return state;
    }
}
