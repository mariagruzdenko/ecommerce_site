import axios from 'axios';
import {
    CART_ADD_ITEM,
    CART_REMOVE_ITEM,
    CART_SAVE_SHIPPING_ADDRESS,
    CART_SAVE_PAYMENT_METHOD,

    CART_NOVAPOSHTA_AREAS_REQUEST,
    CART_NOVAPOSHTA_AREAS_SUCCESS,
    CART_NOVAPOSHTA_AREAS_FAIL,

    CART_NOVAPOSHTA_CITIES_REQUEST,
    CART_NOVAPOSHTA_CITIES_SUCCESS,
    CART_NOVAPOSHTA_CITIES_FAIL,

    CART_NOVAPOSHTA_WAREHOUSES_REQUEST,
    CART_NOVAPOSHTA_WAREHOUSES_SUCCESS,
    CART_NOVAPOSHTA_WAREHOUSES_FAIL,
} from "../constants/cartConstants";

export const addToCart = (id, qty) => async (dispatch, getState) => {
    const {data} = await axios.get(`/api/products/${id}`);

    dispatch({
        type: CART_ADD_ITEM,
        payload:{
            product:data._id,
            name: data.name,
            image: data.image,
            price: data.price,
            countInStock: data.countInStock,
            qty: Number(qty),
        }
    })

    localStorage.setItem("cartItems", JSON.stringify(getState().cart.cartItems))
}


export const removeFromCart = (id) => (dispatch, getState) => {
    dispatch({
        type: CART_REMOVE_ITEM,
        payload: id,
    });

    localStorage.setItem("cartItems", JSON.stringify(getState().cart.cartItems))
}

export const saveShippingAddress = (data) => (dispatch) => {
    dispatch({
        type: CART_SAVE_SHIPPING_ADDRESS,
        payload: data,
    });

    localStorage.setItem("shippingAddress", JSON.stringify(data));
}

export const savePaymentMethod = (data) => (dispatch) => {
    dispatch({
        type: CART_SAVE_PAYMENT_METHOD,
        payload: data,
    });

    localStorage.setItem("paymentMethod", JSON.stringify(data));
}




export const getNovaPoshtaAreas = () => async (dispatch) => {
    try{
        dispatch({type: CART_NOVAPOSHTA_AREAS_REQUEST})

        const config = {
            headers: {
                'Content-Type': 'application/json',
            },

        }

        const {data} = await axios.get(`/api/novaposhta/areas`, config);


        dispatch({
            type: CART_NOVAPOSHTA_AREAS_SUCCESS,
            payload: data,
        })

    }catch(error){
        dispatch({
            type: CART_NOVAPOSHTA_AREAS_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message,
        })
    }
}


export const getNovaPoshtaCities = (area) => async (dispatch) => {
    try{
        dispatch({type: CART_NOVAPOSHTA_CITIES_REQUEST})

        const config = {
            headers: {
                'Content-Type': 'application/json',
            },

        }
        const {data} = await axios.get(`/api/novaposhta/cities/${area}`, config);

        dispatch({
            type: CART_NOVAPOSHTA_CITIES_SUCCESS,
            payload: data,
        })

    }catch(error){
        dispatch({
            type: CART_NOVAPOSHTA_CITIES_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message,
        })
    }
}

export const getNovaPoshtaWarehouses = (city) => async (dispatch) => {
    try{
        dispatch({type: CART_NOVAPOSHTA_WAREHOUSES_REQUEST})

        const config = {
            headers: {
                'Content-Type': 'application/json',
            },

        }
        const {data} = await axios.get(`/api/novaposhta/warehouses/${city}`, config);

        dispatch({
            type: CART_NOVAPOSHTA_WAREHOUSES_SUCCESS,
            payload: data,
        })

    }catch(error){
        dispatch({
            type: CART_NOVAPOSHTA_WAREHOUSES_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message,
        })
    }
}