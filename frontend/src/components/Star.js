import React from 'react';

const Star = ({color, value}) => {
    const star = 'fas fa-star';
    const halfStar = 'fas fa-star-half-alt';
    const emptyStar = 'far fa-star';
    return (
            <span>
                <i style={{color}} className={
                    value === 'star'
                        ? star
                        : value === 'half-star'
                        ? halfStar
                        : emptyStar
                }></i>
            </span>

    );
};

export default Star;