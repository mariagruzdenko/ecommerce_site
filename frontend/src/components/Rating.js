import React from 'react';
import Star from './Star';

const Rating = ({value, text, color}) => {
    let emptyStarNum = [];
    let halfStar = [];
    let starNum = [];
    if(value){
        emptyStarNum = [...Array(5 - Math.floor(value)).keys()];
        halfStar = [...Array(Math.ceil(value % 2)).keys()];
        starNum= [...Array(5 - Math.ceil(value % 2) - (5 - Math.floor(value))).keys()];
    }



    return (
        <div className="rating">
            {starNum.map((i) => <Star key={i+1} color={color} value='star'/>) }
            {halfStar.map((i) => <Star key={i+1} color={color} value='half-star'/>)}
            {emptyStarNum.map((i) => <Star key={i+1} color={color} value='empty-star'/>)}

            {text}
        </div>
    );
};

export default Rating;