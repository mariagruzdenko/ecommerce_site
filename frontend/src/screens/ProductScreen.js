import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams, useNavigate, useLocation } from 'react-router-dom';
import { Row, Col, Image, ListGroup, Button, Card, Form } from 'react-bootstrap';
import Rating from "../components/Rating";
import Loader from "../components/Loader";
import Message from "../components/Message";
import {listProductDetails, createProductReview} from '../actions/productActions';
import { PRODUCT_CREATE_REVIEW_RESET } from '../constants/productConstants';


const ProductScreen = () => {
    const [qty, setQty] = useState(1);
    const [rating, setRating] = useState(0)
    const [comment, setComment] = useState('')

    const navigate = useNavigate();
    const dispatch = useDispatch();


    const productDetails = useSelector(state => state.productDetails)
    const {loading, error, product} = productDetails;

    const params = useParams();

    const userLogin = useSelector(state => state.userLogin)
    const { userInfo } = userLogin

    const productReviewCreate = useSelector(state => state.productReviewCreate)

    const {
        loading: loadingProductReview,
        error: errorProductReview,
        success: successProductReview,
    } = productReviewCreate

    useEffect(() => {
        if (successProductReview) {
            setRating(0)
            setComment('')
            dispatch({ type: PRODUCT_CREATE_REVIEW_RESET })
        }

        dispatch(listProductDetails(params.id))

    }, [dispatch, params, successProductReview])


    const addToCartHandler = () => {
        navigate(`/cart/${params.id}?qty=${qty}`, { replace: true });
    }

    const submitHandler = (e) => {
        e.preventDefault()
        dispatch(createProductReview(
            params.id, {
                rating,
                comment
            }
        ))
    }


    return (
        <div>
            <Link to='/' className='btn btn-light my-3'>Назад</Link>

            {loading ?
                <Loader />
                : error
                    ? <Message variant="danger">{error}</Message>
                    : (
                        <div>
                            <Row>
                                <Col md={6}>
                                    <Image src={product.image} alt={product.name} fluid/>
                                </Col>
                                <Col md={3}>
                                    <ListGroup variant='flush'>
                                        <ListGroup.Item>
                                            <h3>{product.name}</h3>
                                        </ListGroup.Item>
                                        <ListGroup.Item>
                                            <Rating value={product.rating}
                                                    text={`${product.numReviews} reviews`}
                                                    color={'#f8e825'}/>
                                        </ListGroup.Item>
                                        <ListGroup.Item>
                                            Цена: ${product.price}
                                        </ListGroup.Item>
                                        <ListGroup.Item>
                                            Описание: {product.description}
                                        </ListGroup.Item>
                                    </ListGroup>
                                </Col>
                                <Col md={3}>
                                    <Card>
                                        <ListGroup variant='flush'>
                                            <ListGroup.Item>
                                                <Row>
                                                    <Col>Цена: </Col>
                                                    <Col>
                                                        <strong>${product.price}</strong>
                                                    </Col>
                                                </Row>
                                            </ListGroup.Item>

                                            <ListGroup.Item>
                                                <Row>
                                                    <Col>Наличие: </Col>
                                                    <Col>
                                                        {product.countInStock > 0 ? 'Есть в наличии' : 'Нет в наличии'}
                                                    </Col>
                                                </Row>
                                            </ListGroup.Item>

                                            {
                                                product.countInStock > 0 && (
                                                    <ListGroup.Item>
                                                        <Row>
                                                            <Col xs="auto" className='my-1'>Qty: </Col>
                                                            <Col>
                                                                <Form.Control as="select"
                                                                              value={qty}
                                                                              onChange={(e) => setQty(e.target.value)}
                                                                >
                                                                    {
                                                                        [...Array(product.countInStock).keys()].map((x) =>
                                                                            <option key={x + 1} value={x + 1}>{x + 1}</option>)
                                                                    }

                                                                </Form.Control>
                                                            </Col>
                                                        </Row>
                                                    </ListGroup.Item>
                                                )
                                            }
                                            <ListGroup.Item>
                                                <div className="d-grid gap-2">
                                                    <Button
                                                            onClick={addToCartHandler}
                                                            className='btn-lg'
                                                            disabled={product.countInStock === 0}
                                                            type='button'>Добавить в корзину</Button>
                                                </div>
                                            </ListGroup.Item>
                                        </ListGroup>
                                    </Card>
                                </Col>
                            </Row>

                            <Row>
                                <Col md={6}>
                                    <h4>Отзывы</h4>
                                    {product.reviews.length === 0 && <Message variant='info'>Нет отзывов</Message>}

                                    <ListGroup variant='flush'>
                                        {product.reviews.map((review) => (
                                            <ListGroup.Item key={review._id}>
                                                <strong>{review.name}</strong>
                                                <Rating value={review.rating} color='#f8e825' />
                                                <p>{review.createdAt}</p>
                                                <p>{review.comment}</p>
                                            </ListGroup.Item>
                                        ))}

                                        <ListGroup.Item>
                                            <h4>Оставить отзыв</h4>

                                            {loadingProductReview && <Loader />}
                                            {successProductReview && <Message variant='success'>Отзыв оставлен</Message>}
                                            {errorProductReview && <Message variant='danger'>{errorProductReview}</Message>}

                                            {userInfo ? (
                                                <Form onSubmit={submitHandler}>
                                                    <Form.Group controlId='rating'>
                                                        <Form.Label>Rating</Form.Label>
                                                        <Form.Control
                                                            as='select'
                                                            value={rating}
                                                            onChange={(e) => setRating(e.target.value)}
                                                        >
                                                            <option value=''>Выберите...</option>
                                                            <option value='1'>1 - Ужасно</option>
                                                            <option value='2'>2 - Плохо</option>
                                                            <option value='3'>3 - Удовлетворительно</option>
                                                            <option value='4'>4 - Хорошо</option>
                                                            <option value='5'>5 - Отлично</option>
                                                        </Form.Control>
                                                    </Form.Group>

                                                    <Form.Group controlId='comment'>
                                                        <Form.Label>Отзыв</Form.Label>
                                                        <Form.Control
                                                            as='textarea'
                                                            row='5'
                                                            value={comment}
                                                            onChange={(e) => setComment(e.target.value)}
                                                        ></Form.Control>
                                                    </Form.Group>

                                                    <Button
                                                        disabled={loadingProductReview}
                                                        type='submit'
                                                        variant='primary'
                                                    >
                                                        Отправить
                                                    </Button>

                                                </Form>
                                            ) : (
                                                <Message variant='info'>Пожалуйста, <Link to='/login'>войдите</Link>, чтобы оставить отзыв</Message>
                                            )}
                                        </ListGroup.Item>
                                    </ListGroup>
                                </Col>
                            </Row>
                        </div>
                    )
            }


        </div>
    );
};

export default ProductScreen;