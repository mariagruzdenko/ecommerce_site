import React, {useState, useEffect} from 'react';
import {useNavigate} from 'react-router-dom';
import { Form, Button } from 'react-bootstrap';
import Select from 'react-select';
import {useDispatch, useSelector} from 'react-redux';
import FormContainer from "../components/FormContainer";
import CheckoutSteps from "../components/CheckoutSteps";
import {
    saveShippingAddress,
    getNovaPoshtaAreas,
    getNovaPoshtaCities,
    getNovaPoshtaWarehouses
} from "../actions/cartActions";
import Message from "../components/Message";

const ShippingScreen = () => {

    const cart = useSelector(state => state.cart);
    const novaposhta = useSelector(state => state.novaPoshtaDictionaries)
    const { shippingAddress } = cart;
    const { areas, cities, warehouses } = novaposhta;


    const dispatch = useDispatch();
    const navigate = useNavigate();

    const [clientName, setClientName] = useState(shippingAddress.clientName);
    const [phone, setPhone] = useState(shippingAddress.phone);
    const [area, setArea] = useState(shippingAddress.area)
    const [city, setCity] = useState(shippingAddress.city);
    const [warehouse, setWarehouse] = useState(shippingAddress.warehouse);

    let areasOptions = [];
    let citiesOptions = [];
    let warehousesOptions = [];

    useEffect(() => {
        dispatch(getNovaPoshtaAreas());
    }, [dispatch]);

    const submitHandler = (e) =>{
        e.preventDefault();
        dispatch(saveShippingAddress({ clientName, phone, area: area.value, city: city.value, warehouse: warehouse.value}));
        navigate('/payment');
    }

    const areaChangeHandler = (e) => {
        setArea({value: e.value, label: e.value});
        setCity('')
        setWarehouse('')
        dispatch(getNovaPoshtaCities(e.value));

    }

    const cityChangeHandler = (e) => {
        setCity({value: e.value, label: e.value});
        setWarehouse('')
        dispatch(getNovaPoshtaWarehouses(e.value));
    }

    if (!areas.loading && areas.areasList){
        areasOptions = areas.areasList.map(item => {
            return {
                value: item["descriptionRu"],
                label: item["descriptionRu"]
            }
        })
    }

    if (!cities.loading && cities.citiesList){
        citiesOptions = cities.citiesList.map(item => {
            return {
                value: item["descriptionRu"],
                label: item["descriptionRu"]
            }
        })
    }

    if (!warehouses.loading && warehouses.warehousesList){
        warehousesOptions = warehouses.warehousesList.map(item => {
            return {
                value: item["descriptionRu"],
                label: item["descriptionRu"]
            }
        })
    }

    return (
        <FormContainer>
            <CheckoutSteps step1 step2/>
            <h1>Доставка</h1>
            <h3>Выберите адрес новой почты для доставки</h3>

            <Form onSubmit={submitHandler}>
                <Form.Group controlId='clientName'>
                    <Form.Label className='mt-4'>ФИО получателя</Form.Label>
                    <Form.Control
                        required
                        type='text'
                        value={clientName ? clientName : ''}
                        onChange={(e) => setClientName(e.target.value)}>
                    </Form.Control>
                </Form.Group>
                <Form.Group controlId='phoneNumber'>
                    <Form.Label className='mt-4'>Номер телефона</Form.Label>
                    <Form.Control
                        required
                        type='tel'
                        pattern={"\+38\(0[0-9]{2}\)[0-9]{3}-[0-9]{2}-[0-9]{2}"}
                        value={phone ? phone : ''}
                        onChange={(e) => setPhone(e.target.value)}>
                    </Form.Control>
                </Form.Group>
                <Form.Group controlId='shippingArea'>
                    <Form.Label className='mt-4'>Область</Form.Label>
                    {areas.loading ? <Select isLoading={"true"}/>
                        : areas.error ? <Message variant="danger">{areas.error}</Message>
                            :
                            <Select options={areasOptions}
                                    onChange={areaChangeHandler}
                                    value={area}
                                    placeholder={"Выберите область..."}
                                    noOptionsMessage={()=>{ return "Нет вариантов..."}}
                            />
                    }
                </Form.Group>
                <Form.Group controlId='shippingCity'>
                    <Form.Label className='mt-4'>Город</Form.Label>
                        {cities.loading ? <Select isLoading={"true"}/>
                            : cities.error ? <Message variant="danger">{cities.error}</Message>
                                :
                                <Select options={citiesOptions}
                                        onChange={cityChangeHandler}
                                        value={city}
                                        placeholder={"Выберите город..."}
                                        noOptionsMessage={()=>{ return "Нет вариантов..."}}
                                />
                        }
                </Form.Group>
                <Form.Group controlId='shippingWarehouse'>
                    <Form.Label className='mt-4'>Отделение</Form.Label>
                        {warehouses.loading ? <Select isLoading={"true"}/>
                            : warehouses.error ? <Message variant="danger">{warehouses.error}</Message>
                                :
                                <Select options={warehousesOptions}
                                        onChange={setWarehouse}
                                        placeholder={"Выберите отделение..."}
                                        value={warehouse}
                                        noOptionsMessage={()=>{ return "Нет вариантов..."}}
                                />
                        }
                </Form.Group>


                <Button className='mt-3' type="submit" variant='primary'>Продолжить</Button>

            </Form>

        </FormContainer>
    );
};

export default ShippingScreen;