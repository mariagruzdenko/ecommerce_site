import React, {useState, useEffect} from 'react';
import {Link, useLocation, useNavigate} from 'react-router-dom';
import { Form, Button, Row, Col } from 'react-bootstrap';
import {useDispatch, useSelector} from 'react-redux';
import Loader from "../components/Loader";
import Message from "../components/Message";
import { login } from '../actions/userActions';
import FormContainer from "../components/FormContainer";

const LoginScreen = () => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const dispatch = useDispatch();

    const userLogin = useSelector(state => state.userLogin)
    const { error, loading, userInfo } = userLogin;
    const navigate = useNavigate();
    const location = useLocation();
    const redirect = location.search ? location.search.split('=')[1] : '/';


    useEffect(() => {
        if(userInfo){
            navigate(`/${redirect}`);
        }
    }, [userInfo, redirect, navigate])


    const submitHandler = (e) => {
        e.preventDefault();
        dispatch(login(email, password));
    }

    return (
        <FormContainer>
            <h1>Войти</h1>
            {error && <Message variant='danger'>{error}</Message>}
            {loading && <Loader />}
            <Form onSubmit={submitHandler}>
                <Form.Group controlId='email'>
                    <Form.Label>Email Адрес</Form.Label>
                    <Form.Control type='email'
                    placeholder='Введите Email'
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}>

                    </Form.Control>
                </Form.Group>
                <Form.Group controlId='password'>
                    <Form.Label>Пароль</Form.Label>
                    <Form.Control type='password'
                                  placeholder='Введите пароль'
                                  value={password}
                                  onChange={(e) => setPassword(e.target.value)}>

                    </Form.Control>
                </Form.Group>

                <Button className='mt-3' type="submit" variant='primary'>Войти</Button>
            </Form>
            <Row className='py-2'>
                <Col>
                    Новый пользователь ?
                    <Link className='ml-3' to={ redirect ? `/register?redirect=${redirect}` : '/register' }>Зарегистрироваться</Link>
                </Col>
            </Row>
        </FormContainer>
    );
};

export default LoginScreen;