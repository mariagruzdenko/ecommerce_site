from rest_framework.decorators import api_view
from rest_framework.response import Response

from ..models import NovaPoshtaArea, NovaPoshtaCity, NovaPoshtaWarehouse
from ..serializers import NovaPoshtaAreaSerializer, NovaPoshtaCitySerializer, NovaPoshtaWarehouseSerializer


@api_view(['GET'])
def getAreas(request):
    areas = NovaPoshtaArea.objects.all()
    serializer = NovaPoshtaAreaSerializer(areas, many=True)
    return Response(serializer.data)


@api_view(['GET'])
def getCitiesByArea(request, area):
    cities = NovaPoshtaCity.objects.filter(areaDescriptionRu__contains=area)
    serializer = NovaPoshtaCitySerializer(cities, many=True)
    return Response(serializer.data)


@api_view(['GET'])
def getWarehousesByCity(request, city):
    warehouses = NovaPoshtaWarehouse.objects.filter(cityDescriptionRu__contains=city)
    serializer = NovaPoshtaWarehouseSerializer(warehouses, many=True)
    return Response(serializer.data)

