from django.contrib import admin
from django.template.response import TemplateResponse
from django.urls import path
from .models import *


class MyAdminSite(admin.AdminSite):
    site_header = 'BeautyShop'
    site_title = site_header

    def get_urls(self):

        return [
                   path('my_view/', self.admin_view(self.my_view), name="my_view")
               ] + super().get_urls()

    def my_view(self, request):
        data = {"data": "Some data"}
        return TemplateResponse(request, "admin/my_view.html", context=data)

    def get_app_list(self, request):
        apps = [{'name': 'Допольнительные операции',
                 'models': [
                     {'name': 'Обновить справочники Новой Почты',
                      'perms': {'change': True},
                      'admin_url': 'my_view/'
                      }
                 ]

                 }]
        return apps + super().get_app_list(request)


admin_site = MyAdminSite()

# Register your models here.
admin_site.register(Product)
admin_site.register(Review)
admin_site.register(Order)
admin_site.register(OrderItem)
admin_site.register(ShippingAddress)
admin_site.register(NovaPoshtaArea)
admin_site.register(NovaPoshtaCity)
admin_site.register(NovaPoshtaWarehouse)
