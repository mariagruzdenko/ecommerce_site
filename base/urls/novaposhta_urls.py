from django.urls import path
from ..views import novaposhta_views as views


urlpatterns = [
    path('areas', views.getAreas, name='areas'),
    path('cities/<str:area>', views.getCitiesByArea, name='cities'),
    path('warehouses/<str:city>', views.getWarehousesByCity, name='warehouses'),
    # path('<str:pk>', views.getProduct, name='product'),
    # path('<str:pk>/reviews/', views.createProductReview, name="create-review"),
]