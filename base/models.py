from django.db import models
from django.contrib.auth.models import User


class Product(models.Model):
    class Meta:
        db_table = 'base_product'
        verbose_name_plural = 'Товары'
        verbose_name = 'Товар'

    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    name = models.CharField(max_length=200, null=True, blank=True)
    image = models.ImageField(null=True, blank=True)
    brand = models.CharField(max_length=200, null=True, blank=True)
    category = models.CharField(max_length=200, null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    rating = models.DecimalField(max_digits=7, decimal_places=2, null=True, blank=True)
    numReviews = models.IntegerField(null=True, blank=True, default=0)
    price = models.DecimalField(max_digits=7, decimal_places=2, null=True, blank=True)
    countInStock = models.IntegerField(null=True, blank=True, default=0)
    createdAt = models.DateTimeField(auto_now_add=True)
    _id = models.AutoField(primary_key=True, editable=False)

    def __str__(self):
        return self.name


class Review(models.Model):
    class Meta:
        db_table = 'base_review'
        verbose_name_plural = 'Отзывы'
        verbose_name = 'Отзыв'
        ordering = ['-_id']

    product = models.ForeignKey(Product, on_delete=models.SET_NULL, null=True)
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    name = models.CharField(max_length=200, null=True, blank=True)
    rating = models.IntegerField(null=True, blank=True, default=0)
    comment = models.TextField(null=True, blank=True)
    createdAt = models.DateTimeField(auto_now_add=True)
    _id = models.AutoField(primary_key=True, editable=False)

    def __str__(self):
        return str(self.rating)


class Order(models.Model):
    class Meta:
        db_table = 'base_order'
        verbose_name_plural = 'Заказы'
        verbose_name = 'Заказ'
        ordering = ['-_id']

    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    paymentMethod = models.CharField(max_length=200, null=True, blank=True)
    taxPrice = models.DecimalField(max_digits=7, decimal_places=2, null=True, blank=True)
    shippingPrice = models.DecimalField(max_digits=7, decimal_places=2, null=True, blank=True)
    totalPrice = models.DecimalField(max_digits=7, decimal_places=2, null=True, blank=True)
    isPaid = models.BooleanField(default=False)
    paidAt = models.DateTimeField(auto_now_add=False, null=True, blank=True)
    isDelivered = models.BooleanField(default=False)
    deliveredAt = models.DateTimeField(auto_now_add=False, null=True, blank=True)
    createdAt = models.DateTimeField(auto_now_add=True)
    _id = models.AutoField(primary_key=True, editable=False)

    def __str__(self):
        return '#' + str(self._id) + ' ' + str(self.createdAt)


class OrderItem(models.Model):
    class Meta:
        db_table = 'base_orderitem'
        verbose_name_plural = 'Товары заказа'
        verbose_name = 'Товар заказа'

    product = models.ForeignKey(Product, on_delete=models.SET_NULL, null=True)
    order = models.ForeignKey(Order, on_delete=models.SET_NULL, null=True)
    name = models.CharField(max_length=200, null=True, blank=True)
    qty = models.IntegerField(null=True, blank=True, default=0)
    price = models.DecimalField(max_digits=7, decimal_places=2, null=True, blank=True)
    image = models.CharField(max_length=200, null=True, blank=True)
    _id = models.AutoField(primary_key=True, editable=False)

    def __str__(self):
        return str(self.name)


class ShippingAddress(models.Model):
    class Meta:
        db_table = 'base_shippingaddress'
        verbose_name_plural = 'Адреса доставки'
        verbose_name = 'Адрес доставки'

    order = models.OneToOneField(Order, on_delete=models.SET_NULL, null=True, blank=True)
    phoneNumber = models.CharField(max_length=20, null=True, blank=True)
    clientName = models.CharField(max_length=200, null=True, blank=True)
    area = models.CharField(max_length=50, null=True, blank=True)
    city = models.CharField(max_length=50, null=True, blank=True)
    warehouse = models.CharField(max_length=99, null=True, blank=True)
    shippingPrice = models.DecimalField(max_digits=7, decimal_places=2, null=True, blank=True)
    _id = models.AutoField(primary_key=True, editable=False)

    def __str__(self):
        return '#' + str(self.order) + self.area + ' обл., ' + self.city + ', ' + self.warehouse


class NovaPoshtaArea(models.Model):
    class Meta:
        db_table = 'base_novaposhtaarea'
        verbose_name_plural = 'Области Новой Почты'
        verbose_name = 'Область Новой Почты'
        ordering = ['descriptionRu']

    _id = models.AutoField(primary_key=True, editable=False)
    ref = models.CharField(max_length=36, null=False, blank=False)
    areasCenter = models.CharField(max_length=36, null=False, blank=False)
    descriptionRu = models.CharField(max_length=50, null=False, blank=False)
    description = models.CharField(max_length=50, null=False, blank=False)

    def __str__(self):
        return self.descriptionRu


class NovaPoshtaCity(models.Model):
    class Meta:
        db_table = 'base_novaposhtacity'
        verbose_name_plural = 'Города Новой Почты'
        verbose_name = 'Город Новой Почты'
        ordering = ['_id']

    _id = models.AutoField(primary_key=True, editable=False)
    description = models.CharField(max_length=50, null=False, blank=False)
    descriptionRu = models.CharField(max_length=50, null=False, blank=False)
    ref = models.CharField(max_length=36, null=False, blank=False)
    delivery1 = models.CharField(max_length=1, null=False, blank=False)
    delivery2 = models.CharField(max_length=1, null=False, blank=False)
    delivery3 = models.CharField(max_length=1, null=False, blank=False)
    delivery4 = models.CharField(max_length=1, null=False, blank=False)
    delivery5 = models.CharField(max_length=1, null=False, blank=False)
    delivery6 = models.CharField(max_length=1, null=False, blank=False)
    delivery7 = models.CharField(max_length=1, null=False, blank=False)
    settlementType = models.CharField(max_length=36, null=False, blank=False)
    cityId = models.CharField(max_length=36, null=False, blank=False)
    settlementTypeDescriptionRu = models.CharField(max_length=36, null=False, blank=False)
    settlementTypeDescription = models.CharField(max_length=36, null=False, blank=False)
    areaDescription = models.CharField(max_length=36, null=False, blank=False)
    areaDescriptionRu = models.CharField(max_length=36, null=False, blank=False)

    def __str__(self):
        return self.descriptionRu + "(" + self.areaDescriptionRu + " обл.)"


class NovaPoshtaWarehouse(models.Model):
    class Meta:
        db_table = 'base_novaposhtawarehouse'
        verbose_name_plural = 'Отделения Новой Почты'
        verbose_name = 'Отделение Новой Почты'
        ordering = ['_id']

    _id = models.AutoField(primary_key=True, editable=False)
    siteKey = models.CharField(max_length=10, null=False, blank=False)
    description = models.CharField(max_length=99, null=False, blank=False)
    descriptionRu = models.CharField(max_length=99, null=False, blank=False)
    shortAddress = models.CharField(max_length=99, null=False, blank=False)
    shortAddressRu = models.CharField(max_length=99, null=False, blank=False)
    typeOfWarehouse = models.CharField(max_length=36, null=False, blank=False)
    ref = models.CharField(max_length=36, null=False, blank=False)
    number = models.CharField(max_length=10, null=False, blank=False)
    cityRef = models.CharField(max_length=36, null=False, blank=False)
    cityDescription = models.CharField(max_length=50, null=False, blank=False)
    cityDescriptionRu = models.CharField(max_length=50, null=False, blank=False)
    settlementRef = models.CharField(max_length=36, null=False, blank=False)
    settlementDescription = models.CharField(max_length=36, null=False, blank=False)
    settlementAreaDescription = models.CharField(max_length=36, null=False, blank=False)
    settlementRegionsDescription = models.CharField(max_length=36, null=False, blank=False)
    settlementTypeDescription = models.CharField(max_length=36, null=False, blank=False)
    settlementTypeDescriptionRu = models.CharField(max_length=36, null=False, blank=False)
    regionCity = models.CharField(max_length=36, null=True, blank=True)
    postalCodeUA = models.CharField(max_length=36, null=False, blank=False)
    warehouseIndex = models.CharField(max_length=36, null=False, blank=False)

    def __str__(self):
        return self.descriptionRu + "(" + self.cityDescriptionRu + ")"

